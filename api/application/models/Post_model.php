<?php
/**
 * Created by PhpStorm.
 * User: ragasub
 * Date: 7/28/17
 * Time: 6:36 PM
 */

use Firebase\JWT\JWT;

class Post_model extends CI_Model
{

    private $token, $decoded_token;

    function __construct()
    {
        parent::__construct();

        if (isset($_SERVER['REDIRECT_HTTP_AUTHORIZATION'])) {
            $this->token = $_SERVER['REDIRECT_HTTP_AUTHORIZATION'];
        } else if (isset(apache_request_headers()['Authorization'])) {
            $this->token = apache_request_headers()['Authorization'];
        } else {
            $this->token = null;
        }
        if (!empty($this->token)) {
            $this->token = explode(" ", $this->token);
            if(count($this->token) > 1) {
            $this->token = $this->token[1];
            } else {
                $this->token = $this->token[0];
            }
            try {
                $this->decoded_token = JWT::decode($this->token, 'examraga', ['HS256']);
            } catch (\Exception $e) {
                $this->decoded_token = (object)['error' => 'Token was tampered, please try relogin'];
            }
        } else {
            $this->decoded_token = (object)['error' => 'Not authorized, please login first'];
        }
    }

    function get_post($id) {
        if (!empty($this->token) && isset($this->decoded_token)) {
            $this->db->select('posts.id, title, users.username, users.name, information, subject, topic, media, location, date, response.response');
            $this->db->from('posts');
            $this->db->join('users', 'posts.id_user = users.id');
            $this->db->join('response', 'posts.id = response.id_post');
            $this->db->where('posts.id', $id);
            
            $result = $this->db->get();

            if($result->num_rows()>0) {
                $data = $result->row();
                $data->topic = json_decode($data->topic);
                $data->subject = json_decode($data->subject);
                $data->media = json_decode($data->media);

                return $data;
            } else {
                return ['error' => 'Data tidak ditemukan'];
            }
        } else {
            return (object)['error' => 'Your token was expired or tampered'];
        }            
    }

    function post_list($start, $count)
    {
        if (!empty($this->token) && isset($this->decoded_token)) {
            $level = $this->decoded_token->level;

            $this->db->select('posts.id, title, users.username, users.name, information, subject, topic, media, location, date, response.response');
            $this->db->from('posts');
            $this->db->join('users', 'posts.id_user = users.id');
            $this->db->join('response', 'posts.id = response.id_post', 'left');     
            $this->db->order_by("posts.id", "desc");

            if ($level > 30) {
                $this->db->where('posts.id_user', $this->decoded_token->id);
            } else if ($level == 21) {
                $this->db->like('topic', 'kurikulum');
            } else if ($level == 22) {
                $this->db->like('topic', 'kesiswaan');
            } else if ($level == 23) {
                $this->db->like('topic', 'sarana');
            } else if ($level == 24) {
                $this->db->like('topic', 'humas');
            }

            $total = clone $this->db;

            // $this->db->limit($count, $start);

            $temp_data = $this->db->get()->result_array();
            $data = [];
            foreach ($temp_data as $key => $value) {
                $value['media'] = json_decode($value['media']);
                $value['subject'] = json_decode($value['subject']);
                $value['topic'] = json_decode($value['topic']);
                array_push($data, $value);
            }
            return ['data' => $data, 'total' => $total->count_all_results()];
        }
    }

    function delete_post($id)
    {
        if (isset($this->decoded_token)) {
            if ($this->decoded_token->level < 30) {
                $post = $this->db->get_where('posts', ['id' => $id]);
                if ($post->num_rows() > 0) {
                    $media = json_decode($post->row()->media);

                    if (count($media->files) > 0) {
                        for ($i = 0; $i < count($media->files); $i++) {
                            unlink(FCPATH . 'uploads/' . $media->files[$i]);
                        }
                    }

                    $this->db->delete('posts', ['id' => $id]);

                    return (object)['success' => true];
                } else {
                    return (object)['error' => 'Data tidak ditemukan'];
                }
            } else {
                if ($this->db->get_where('posts', ['id' => $id, 'id_user' => $this->decoded_token->id])->num_rows() > 0) {
                    $this->db->delete('posts', ['id' => $id]);

                    return (object)['success' => true];
                } else {
                    return (object)['error' => 'Data tidak ditemukan'];
                }
            }
        } else {
            return (object)['error' => 'Your token was expired or tampered'];
        }
    }

    function create_post($title, $location, $information, $topic, $subject, $media)
    {
        if (strlen($title) <= 0) {
            return (object)['error' => 'Mohon isi judul!'];
        } else if (strlen($location) <= 0) {
            return (object)['error' => 'Mohon isi lokasi!'];
        } else if (count($topic) <= 0) {
            return (object)['error' => 'Mohon pilih topik'];
        } else if (count($subject) <= 0) {
            return (object)['error' => 'Mohon pilih subjek!'];
        } else if (strlen($information) <= 0 && count($media['files']) <= 0) {
            return (object)['error' => 'Mohon isi keterangan!'];
        } else {
            if (isset($this->decoded_token)) {
                $user_id = $this->decoded_token->id;
                $insert = $this->db->insert('posts', [
                    'title' => $title,
                    'location' => $location,
                    'information' => $information,
                    'topic' => json_encode($topic),
                    'subject' => json_encode($subject),
                    'media' => json_encode($media),
                    'id_user' => $user_id,
                    'date' => date('c')
                ]);

                if ($insert) {
                    return (object)['success' => true];
                } else {
                    return (object)['error' => $this->db->_error_message()];
                }

            } else {
                return (object)['error' => 'Your token was expired or tampered'];
            }
        }
    }

    function update_status($id, $status)
    {
        if (isset($this->decoded_token)) {
            if ($this->decoded_token->level < 30) {
                if ($this->db->get_where('posts', ['id' => $id])->num_rows() > 0) {
                    $update = $this->db->update('posts', [
                        'status' => strtolower($status)
                    ], ['id' => $id]);

                    if ($update) {
                        return (object)['success' => true];
                    } else {
                        return (object)['error' => $this->db->_error_message()];
                    }
                } else {
                    return (object)['error' => 'Kiriman tidak ditemukan'];
                }
            } else {
                return (object)['error' => 'Tidak berhak untuk mengakses'];
            }
        }
    }
}