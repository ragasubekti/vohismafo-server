<?php
/**
 * Created by PhpStorm.
 * User: ragasub
 * Date: 8/13/17
 * Time: 1:34 PM
 */

use Firebase\JWT\JWT;

class Activation_model extends CI_Model {
    private $token, $decoded_token;

    function __construct()
    {
        parent::__construct();

        if (isset($_SERVER['REDIRECT_HTTP_AUTHORIZATION'])) {
            $this->token = $_SERVER['REDIRECT_HTTP_AUTHORIZATION'];
        } else if (isset(apache_request_headers()['Authorization'])) {
            $this->token = apache_request_headers()['Authorization'];
        } else {
            $this->token = null;
        }
        if (!empty($this->token)) {
            $this->token = explode(" ", $this->token);
            if(count($this->token) > 1) {
            $this->token = $this->token[1];
            } else {
                $this->token = $this->token[0];
            }
            try {
                $this->decoded_token = JWT::decode($this->token, 'examraga', ['HS256']);
            } catch (\Exception $e) {
                $this->decoded_token = (object)['error' => 'Token was tampered, please try relogin'];
            }
        } else {
            $this->decoded_token = (object)['error' => 'Not authorized, please login first'];
        }
    }

    public function get_user($query) {
        if(isset($this->decoded_token)) {
            if($this->decoded_token->level >= 20) {
                return (object)['error' => 'Anda tidak berhak mengakses halaman ini'];
            } else {
                if(strlen($query) > 0) {
                    $this->db->select('username');
                    $this->db->from('users');
                    $this->db->like('username', $query);

                    $total = clone $this->db;

                    $this->db->limit(10, 0);

                    $data = array_map (function($value){
                        return $value['username'];
                    } , $this->db->get()->result_array());

                    return (object)['data' => $data, 'total' => $total->count_all_results()];
                } else {
                    return (object)['data' => [], 'total' => 0];
                }
            }
        } else {
            return (object)['error' => 'Your token was expired or tampered'];
        }
    }

    public function activate_user($username, $code) {
        if(isset($this->decoded_token)) {
            if($this->decoded_token->level >= 20) {
                return (object)['error' => 'Anda tidak berhak mengakses halaman ini'];
            } else {
                $query = $this->db->get_where('users', ['username' => strtolower($username)]);
                if($query->num_rows() > 0) {
                    $result = $query->row();
                    if($result->active > 0) {
                        return (object)['error' => 'Pengguna telah aktif'];
                    } else {
                        if(strtolower($result->activation_code) == strtolower($code)) {
                            $this->db->set('active', true);
                            $this->db->where('username', strtolower($username));
                            $this->db->update('users');

                            return (object)['success' => true];
                        } else {
                            return (object)['error' => 'Kode aktivasi salah'];
                        }
                    }
                } else {
                    return (object)['error' => 'Data tidak ditemukan'];
                }
            }
        } else {
            return (object)['error' => 'Your token was expired or tampered'];            
        }
    }
}