<?php

/**
 * Created by PhpStorm.
 * User: ragasub
 * Date: 7/26/17
 * Time: 7:05 PM
 */

use Firebase\JWT\JWT;

class User_model extends CI_Model
{
    private $token, $decoded_token;

    function __construct()
    {
        parent::__construct();
        if (isset($_SERVER['REDIRECT_HTTP_AUTHORIZATION'])) {
            $this->token = $_SERVER['REDIRECT_HTTP_AUTHORIZATION'];
        } else if (isset(apache_request_headers()['Authorization'])) {
            $this->token = apache_request_headers()['Authorization'];
        } else {
            $this->token = null;
        }
        if (!empty($this->token)) {
            $this->token = explode(" ", $this->token);
            if(count($this->token) > 1) {
            $this->token = $this->token[1];
            } else {
                $this->token = $this->token[0];
            }
            try {
                $this->decoded_token = JWT::decode($this->token, 'examraga', ['HS256']);
            } catch (\Exception $e) {
                return (object)['error' => 'Token was tampered, please try relogin'];
            }
        }
    }

    /**
     * @param $username
     * @param $password
     * @return array|object
     */
    function user_authenticate($username, $password)
    {
        $username = strtolower($username);
        $check = $this->db->get_where('users', ['username' => $username])->num_rows();
        if ($check) {
            $this->db->select('password, active, activation_code');
            $hash = $this->db->get_where('users', ['username' => $username])->row();

            if ($hash->active > 0) {

                if (password_verify($password, $hash->password)) {
                    $this->db->select('id, username, level, name, active, email, phone');
                    $data = $this->db->get_where('users', ['username' => $username])->row();
                    return [
                        'token' => JWT::encode($data, 'examraga'),
                        'username' => $data->username,
                        'name' => $data->name,
                        'user_level' => $data->level,
                    ];
                } else {
                    return ['error' => 'Username atau kata sandi salah'];
                }

            } else {
                return (object)['error' => 'Akun belum diaktifkan, silahkan hubungi TU.', $hash];
            }


        } else {
            return ['error' => 'Username atau kata sandi salah'];
        }
    }

    /**
     * @param $start
     * @param $count
     * @return array|object
     */
    function user_list($start, $count)
    {
        if (isset($this->decoded_token)) {
            if($this->decoded_token->level < 20) {
                $this->db->select('id, username, level, name, active, email, phone');
                $this->db->from('users');
                $db = clone $this->db; // clone for total
    
                $this->db->limit($count, $start);
    
                $result = $this->db->get()->result();
                $total = $db->count_all_results();
    
                return $data = ['data' => $result, 'total' => $total];
            } else {
                return (object)['error' => 'Anda tidak berhak mengakses halaman ini'];
            }
        
        } else {
            return (object)['error' => 'Your token was expired or tampered'];
        }
    }

    /**
     * @param $id
     * @return object
     */
    function user_delete($id)
    {
        if (isset($this->decoded_token)) {
            $level = $this->decoded_token->level;
            if (($level == 0) || ($level == 00) || ($level == 10)) {
                if ($this->db->get_where('users', ['id' => $id])->num_rows() > 0) {
                    $this->db->delete('users', ['id' => $id]);
                    return (object)['success' => true];
                } else {
                    return (object)['error' => 'Data tidak ditemukan atau sudah terhapus'];
                }
            } else {
                return (object)['error' => 'Authentikasi gagal'];
            }
        } else {
            return (object)['error' => 'Your token was expired or tampered'];
        }
    }

    /**
     * @param $username
     * @param $name
     * @param $phone
     * @param $email
     * @param $password
     * @return object
     */
    function user_signup($username, $password, $name, $phone, $email)
    {
        if (strlen($username) <= 0) {
            return (object)['error' => 'NIS/Username tidak boleh kosong'];
        } else if (strlen($name) <= 0) {
            return (object)['error' => 'Nama tidak boleh kosong!'];
        } else if (strlen($phone) <= 0) {
            return (object)['error' => 'No. HP wajib diisi!'];
        } else if (strlen($email) <= 0) {
            return (object)['error' => 'Email wajib diisi!'];
        } else {
            // do the magic
            if ($this->db->get_where('users', ['username' => strtolower($username)])->num_rows() > 0) {
                return (object)['error' => 'NIS/Username telah digunakan, hubungi TU jika kamu lupa kata sandi'];
            } else {
                $code = $this->randomString(6);
                $this->db->insert('users', [
                    'username' => strtolower($username),
                    'name' => strtoupper($name),
                    'phone' => $phone,
                    'email' => $email,
                    'level' => 33,
                    'activation_code' => strtolower($code),
                    'password' => password_hash($password, PASSWORD_DEFAULT)]);

                return (object)['success' => true, 'code' => strtoupper($code)];
            }
        }
    }

    function admin_signup($username, $password, $name, $phone, $email, $level) {
        if(isset($this->decoded_token)) {
            if (strlen($username) <= 0) {
                return (object)['error' => 'NIS/Username tidak boleh kosong'];
            } else if (strlen($name) <= 0) {
                return (object)['error' => 'Nama tidak boleh kosong!'];
            } else if (strlen($phone) <= 0) {
                return (object)['error' => 'No. HP wajib diisi!'];
            } else if (strlen($email) <= 0) {
                return (object)['error' => 'Email wajib diisi!'];
            } else {
                if ($this->db->get_where('users', ['username' => strtolower($username)])->num_rows() > 0) {
                    return (object)['error' => 'NIS/Username telah digunakan.'];
                } else {
                    $code = $this->randomString(6);
                    $this->db->insert('users', [
                        'username' => strtolower($username),
                        'name' => strtoupper($name),
                        'phone' => $phone,
                        'email' => $email,
                        'level' => $level,
                        'active' => true,
                        'activation_code' => strtolower($code),
                        'password' => password_hash($password, PASSWORD_DEFAULT)
                    ]);
                    return (object)['success' => true, 'code' => strtoupper($code)];
                }
            }
        } else {
            return ['error' => 'Your token was expired or tampered'];
        }
    }

    /**
     * @param $length
     * @return string
     */
    private function randomString($length)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}