<?php
/**
 * Created Date: Wednesday, August 16th 2017, 11:45:33 am
 * Author: Raga Subekti
 * -----
 * Last Modified: Wed Aug 16 2017
 * Modified By: Raga Subekti
 * -----
 */

class Statistic_model extends CI_Model
{

    private $token, $decoded_token;

    function __construct()
    {
        parent::__construct();

        if (isset($_SERVER['REDIRECT_HTTP_AUTHORIZATION'])) {
            $this->token = $_SERVER['REDIRECT_HTTP_AUTHORIZATION'];
        } else if (isset(apache_request_headers()['Authorization'])) {
            $this->token = apache_request_headers()['Authorization'];
        } else {
            $this->token = null;
        }
        if (!empty($this->token)) {
            $this->token = explode(" ", $this->token);
            if (count($this->token) > 1) {
                $this->token = $this->token[1];
            } else {
                $this->token = $this->token[0];
            }
            try {
                $this->decoded_token = JWT::decode($this->token, 'examraga', ['HS256']);
            } catch (\Exception $e) {
                $this->decoded_token = (object)['error' => 'Token was tampered, please try relogin'];
            }
        } else {
            $this->decoded_token = (object)['error' => 'Not authorized, please login first'];
        }
    }

    function subject_statistic($option = 'today')
    {
//        if(isset($this->decoded_token) && $this->decoded_token->level < 20) {
        if ($option == 'today') {
            return [
                'keluhan' => $this->keluhan_today(),
                'saran' => $this->saran_today(),
                'pengumuman' => $this->pengumuman_today()
            ];
        } else if ($option == 'this_month') {
            return [
                'keluhan' => $this->keluhan_this_month(),
                'saran' => $this->saran_this_month(),
                'pengumuman' => $this->pengumuman_this_month()
            ];

        } else if ($option == 'monthly') {
            return [
                'keluhan' => $this->keluhan_monthly(),
                'saran' => $this->saran_monthly(),
                'pengumuman' => $this->pengumuman_monthly()
            ];
        }


//        } else {
//            return (object)['error' => 'Anda tidak berhak mengakses halaman ini'];
//        }
    }

    private function pengumuman_today()
    {
        $fn = new DateTime();
        $this_month = $fn->format('Ymd');
        $sql = "SELECT COUNT(*) AS pengumuman FROM posts WHERE subject LIKE '%pengumuman%' AND DATE_FORMAT(date,'%Y%m%d')='{$this_month}'";

        return $this->db->query($sql)->row()->pengumuman;
    }

    private function saran_today()
    {
        $fn = new DateTime();
        $this_month = $fn->format('Ymd');
        $sql = "SELECT COUNT(*) AS saran FROM posts WHERE subject LIKE '%saran%' AND DATE_FORMAT(date,'%Y%m%d')='{$this_month}'";

        return $this->db->query($sql)->row()->saran;
    }

    private function keluhan_today()
    {
        $fn = new DateTime();
        $this_month = $fn->format('Ymd');
        $sql = "SELECT COUNT(*) AS keluhan FROM posts WHERE subject LIKE '%keluhan%' AND DATE_FORMAT(date,'%Y%m%d')='{$this_month}'";

        return $this->db->query($sql)->row()->keluhan;
    }

    private function pengumuman_this_month()
    {
        $fn = new DateTime();
        $this_month = $fn->format('Ym');
        $sql = "SELECT COUNT(*) AS pengumuman FROM posts WHERE subject LIKE '%pengumuman%' AND DATE_FORMAT(date,'%Y%m')='{$this_month}'";

        return $this->db->query($sql)->row()->pengumuman;
    }

    private function saran_this_month()
    {
        $fn = new DateTime();
        $this_month = $fn->format('Ym');
        $sql = "SELECT COUNT(*) AS saran FROM posts WHERE subject LIKE '%saran%' AND DATE_FORMAT(date,'%Y%m')='{$this_month}'";

        return $this->db->query($sql)->row()->saran;
    }

    private function keluhan_this_month()
    {
        $fn = new DateTime();
        $this_month = $fn->format('Ym');
        $sql = "SELECT COUNT(*) AS keluhan FROM posts WHERE subject LIKE '%keluhan%' AND DATE_FORMAT(date,'%Y%m')='{$this_month}'";

        return $this->db->query($sql)->row()->keluhan;
    }

    private function keluhan_monthly()
    {
        $fn = new DateTime();
        $this_month = $fn->format('Ym');
        $sql = "
          SELECT DATE_FORMAT(date,'%d') AS date,COUNT(*) AS keluhan
          FROM posts
          WHERE subject LIKE '%keluhan%' AND DATE_FORMAT(date,'%Y%m') = '{$this_month}'
          GROUP BY DATE_FORMAT(date,'%Y%m%d')
        ";

        $query = $this->db->query($sql);
        return $query->result();
    }

    private function saran_monthly()
    {
        $fn = new DateTime();
        $this_month = $fn->format('Ym');
        $sql = "
          SELECT DATE_FORMAT(date,'%d') AS date,COUNT(*) AS saran
          FROM posts
          WHERE subject LIKE '%saran%' AND DATE_FORMAT(date,'%Y%m') = '{$this_month}'
          GROUP BY DATE_FORMAT(date,'%Y%m%d')
        ";

        $query = $this->db->query($sql);
        return $query->result();
    }

    private function pengumuman_monthly()
    {
        $fn = new DateTime();
        $this_month = $fn->format('Ym');
        $sql = "
          SELECT DATE_FORMAT(date,'%d') AS date,COUNT(*) AS pengumuman
          FROM posts
          WHERE subject LIKE '%pengumuman%' AND DATE_FORMAT(date,'%Y%m') = '{$this_month}'
          GROUP BY DATE_FORMAT(date,'%Y%m%d')
        ";

        $query = $this->db->query($sql);
        return $query->result();
    }

}