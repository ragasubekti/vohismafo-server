<?php

/**
 * Created by PhpStorm.
 * User: ragasub
 * Date: 7/26/17
 * Time: 7:05 PM
 */

use Firebase\JWT\JWT;

class Response_model extends CI_Model {
    private $token, $decoded_token;
    
    function __construct()
    {
        parent::__construct();
        if (isset($_SERVER['REDIRECT_HTTP_AUTHORIZATION'])) {
            $this->token = $_SERVER['REDIRECT_HTTP_AUTHORIZATION'];
        } else if (isset(apache_request_headers()['Authorization'])) {
            $this->token = apache_request_headers()['Authorization'];
        } else {
            $this->token = null;
        }
        if (!empty($this->token)) {
            $this->token = explode(" ", $this->token);
            if(count($this->token) > 1) {
            $this->token = $this->token[1];
            } else {
                $this->token = $this->token[0];
            }
            try {
                $this->decoded_token = JWT::decode($this->token, 'examraga', ['HS256']);
            } catch (\Exception $e) {
                return (object)['error' => 'Token was tampered, please try relogin'];
            }
        }
    }

    function get_response($id) {
        if (!empty($this->token) && isset($this->decoded_token)) {
            $this->db->select('response, comment, created_at as created, updated_at as update, users.name');
            $this->db->join('users', 'response.id_user = users.id');            
            $check = $this->db->get_where('response', ['id_post' => $id]);
            if($check->num_rows() > 0) {
                return $check->row();  
            } else {
                return ['error' => 'No data was found in database'];
            }
        } else {
            return (object)['error' => 'Your token was expired or tampered'];            
        }            
    }

    function post_response($post, $response, $comment) {
        if (!empty($this->token) && isset($this->decoded_token)) {
            if($this->decoded_token->level > 30) {
                return (object)['error' => 'Anda tidak berhak mengakses halaman ini'];
            } else {
                $parent = $this->db->get_where('posts', ['id' => $post]);
                if($parent->num_rows() > 0) {
                    $check = $this->db->get_where('response', ['id_post' => $post]);
                    if($check->num_rows() <= 0) {
                        $id_user = $this->decoded_token->id;
                        $this->db->insert('response', [
                            'id_user' => $id_user,
                            'id_post' => $post,
                            'response' => $response,
                            'comment' => $comment,
                            'created_at' => date('c')
                        ]);
                        return ['success'=>true];
                    } else {
                        $real = $check->row();
                        $this->db->set(['response' => $response, 'comment' => $comment, 'updated_at' => date('c')]);
                        $this->db->where('id', $real->id);
                        $this->db->update('response');
                        return ['success'=>true];   
                    }
                } else {
                    return (object)['error' => 'Data tidak ditemukan'];
                }
            }
        } else {
            return (object)['error' => 'Your token was expired or tampered'];            
        }
    }
}