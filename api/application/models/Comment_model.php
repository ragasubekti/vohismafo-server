<?php
/**
 * Created by PhpStorm.
 * User: ragasub
 * Date: 8/12/17
 * Time: 12:12 AM
 */

use Firebase\JWT\JWT;

class Comment_model extends CI_Model
{

    private $token, $decoded_token;

    function __construct()
    {
        parent::__construct();

        if (isset($_SERVER['REDIRECT_HTTP_AUTHORIZATION'])) {
            $this->token = $_SERVER['REDIRECT_HTTP_AUTHORIZATION'];
        } else if (isset(apache_request_headers()['Authorization'])) {
            $this->token = apache_request_headers()['Authorization'];
        } else {
            $this->token = null;
        }
        if (!empty($this->token)) {
            $this->token = explode(" ", $this->token);
            if(count($this->token) > 1) {
            $this->token = $this->token[1];
            } else {
                $this->token = $this->token[0];
            }
            try {
                $this->decoded_token = JWT::decode($this->token, 'examraga', ['HS256']);
            } catch (\Exception $e) {
                $this->decoded_token = (object)['error' => 'Token was tampered, please try relogin'];
            }
        } else {
            $this->decoded_token = (object)['error' => 'Not authorized, please login first'];
        }
    }

    public function comment_post($post, $comment)
    {
        if (isset($this->decoded_token)) {
            if(strlen($comment) <= 0) {
                return (object)['error' => 'Mohon isi komentar!'];
            } else {
                $id_user = $this->decoded_token->id;
                $this->db->insert('comments', [
                    'id_post' => $post,
                    'id_user' => $id_user,
                    'comment' => $comment
                ]);

                return (object)['success' => true];
            }
        } else {
            return (object)['error' => 'Your token was expired or tampered'];
        }
    }

    public function comment_delete($id)
    {
        if (isset($this->decoded_token)) {
            $post = $this->db->get('comments', ['id' => $id]);
            if($post->num_rows() > 0) {
                if($post->row()->id_user == $this->decoded_token->id) {
                    $this->db->delete('comments', ['id' => $id]);

                } else {
                    return (object)['error' => 'Anda tidak berhak menghapus komentar ini'];
                }
            }
        } else {
            return (object)['error' => 'Your token was expired or tampered'];
        }
    }

    public function comment_update($id, $comment)
    {

    }

}