<?php
/**
 * Created by PhpStorm.
 * User: ragas
 * Date: 7/31/17
 * Time: 9:36 AM
 */

use Firebase\JWT\JWT;

class Upload extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if (isset($_SERVER['REDIRECT_HTTP_AUTHORIZATION'])) {
            $this->token = $_SERVER['REDIRECT_HTTP_AUTHORIZATION'];
        } else if (isset(apache_request_headers()['Authorization'])) {
            $this->token = apache_request_headers()['Authorization'];
        } else {
            $this->token = null;
        }
        if (!empty($this->token)) {
            $this->token = explode(" ", $this->token);
            $this->token = $this->token[1];
            try {
                $this->decoded_token = JWT::decode($this->token, 'examraga', ['HS256']);
            } catch (\Exception $e) {
                $this->decoded_token = (object)['error' => 'Token was tampered, please try relogin'];
            }
        } else {
            $this->decoded_token = (object)['error' => 'Not authorized, please login first'];
        }
    }

    function index()
    {
        $this->load->library('upload');
        if (isset($this->decoded_token)) {
            if (!empty($_FILES['file']['name'])) {
                $file = [];
                $filesCount = count($_FILES['file']['name']);
                for ($i = 0; $i < $filesCount; $i++) {
                    // echo json_encode(['error' => $_FILES]);
                    $_FILES['userFile']['name'] = $_FILES['file']['name'][$i];
                    $_FILES['userFile']['type'] = $_FILES['file']['type'][$i];
                    $_FILES['userFile']['tmp_name'] = $_FILES['file']['tmp_name'][$i];
                    $_FILES['userFile']['error'] = $_FILES['file']['error'][$i];
                    $_FILES['userFile']['size'] = $_FILES['file']['size'][$i];

                    $config['upload_path'] = FCPATH . 'uploads/';
                    $config['allowed_types'] = 'gif|jpg|png|jpeg|pdf|mp4|mkv|3gp|3gpp';
                    $config['max_size'] = 0;
                    $config['encrypt_name'] = true;
        
                    $this->upload->initialize($config);

                    if (!$this->upload->do_upload('userFile')) {
                        echo json_encode(['error' => $this->upload->display_errors()]);
                    } else {
                        array_push($file, $this->upload->data()['file_name']);
                    }
                    if ($i == ($filesCount - 1)) {
                        header('Content-Type: application/json');
                        echo json_encode($file);
                    }
                }
            } else {
                header('Content-Type: application/json');
                echo json_encode(['error' => 'Tidak ada file yang diupload']);
            }
        }
    }

}