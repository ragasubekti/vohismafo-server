<?php
/**
 * Created by PhpStorm.
 * User: ragasub
 * Date: 8/13/17
 * Time: 1:34 PM
 */

require(APPPATH . 'libraries/REST_Controller.php');

class Activation extends \Restserver\Libraries\REST_Controller
{

    public function __construct() {
        parent::__construct();

        $this->load->model('Activation_model');
    }

    public function index_get() {
        $query = $this->get('query');
        $this->response($this->Activation_model->get_user($query));
    }

    public function index_post() {
        $this->response(['error' => 'Invalid request']);
    }

    public function index_put() {
        $username = $this->put('username');
        $code = $this->put('code');
        $this->response($this->Activation_model->activate_user($username, $code));
    }

    public function index_delete() {
        $this->response(['error' => 'Invalid request']);        
    }
}