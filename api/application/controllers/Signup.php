<?php
/**
 * Created by PhpStorm.
 * User: ragasub
 * Date: 8/11/17
 * Time: 8:55 PM
 */

require(APPPATH . 'libraries/REST_Controller.php');


class Signup extends \Restserver\Libraries\REST_Controller
{

    function __construct()
    {
        parent::__construct();

        $this->load->model('User_model');
    }

    function index_post()
    {
        $username = $this->post('username');
        $password = $this->post('password');
        $name = $this->post('name');
        $phone = $this->post('phone');
        $email = $this->post('email');

        $this->response($this->User_model->user_signup($username, $password, $name, $phone, $email));
    }

    function index_get()
    {
        $this->response(['error' => 'Invalid request']);
    }

    function index_delete()
    {
        $this->response(['error' => 'Invalid request']);
    }

    function index_put()
    {
        $this->response(['error' => 'Invalid request']);
    }
}