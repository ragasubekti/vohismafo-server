<?php
/**
 * User: ragasub
 */

require(APPPATH . 'libraries/REST_Controller.php');

class Get_Post extends \Restserver\Libraries\REST_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('Post_model');
    }

    public function index_get() {
        $id = $this->get('id');
        $this->response($this->Post_model->get_post($id));
    }
    public function index_post() {
        $this->response(['error' => 'Invalid request']);        
    }
    public function index_put() {
        $this->response(['error' => 'Invalid request']);                
    }
    public function index_delete() {
        $this->response(['error' => 'Invalid request']);                
    }
            
}