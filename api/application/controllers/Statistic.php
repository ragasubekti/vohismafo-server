<?php
/**
 * Created Date: Wednesday, August 16th 2017, 11:35:04 am
 * Author: Raga Subekti
 * -----
 * Last Modified: Wed Aug 16 2017
 * Modified By: Raga Subekti
 * -----
 */

require(APPPATH . 'libraries/REST_Controller.php');
 
class Statistic extends \Restserver\Libraries\REST_Controller {
    function __construct()
    {
        parent::__construct();

        $this->load->model('Statistic_model');
    }

    public function index_get() {
        $filter = $this->get('filter');
        $this->response($this->Statistic_model->subject_statistic($filter));
    }
}