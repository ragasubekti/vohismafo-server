<?php
/**
 * Created by PhpStorm.
 * User: ragasub
 * Date: 7/26/17
 * Time: 6:19 PM
 */

require(APPPATH.'libraries/REST_Controller.php');

class User extends \Restserver\Libraries\REST_Controller {
    function __construct(){
        parent::__construct();

        $this->load->model('User_model');
    }

    function index_get() {
        $result = $this->User_model->user_list(0, 100);
//        $http = isset($result->error) ? 401 : 200;
        $this->response($result);
    }

    function index_post() {
        $username = $this->post('username');
        $name = $this->post('name');
        $phone = $this->post('phone');
        $email = $this->post('email');
        $password = $this->post('password');
        $level = $this->post('level');

        $this->response($this->User_model->admin_signup($username, $password, $name, $phone, $email, $level));
    }

    function index_delete($id) {
        $this->response($this->User_model->user_delete($id));
    }
}
