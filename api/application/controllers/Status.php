<?php
/**
 * Created by PhpStorm.
 * User: ragasub
 * Date: 8/1/17
 * Time: 9:44 PM
 */
require(APPPATH.'libraries/REST_Controller.php');

class Status extends \Restserver\Libraries\REST_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Post_model');
    }

    function index_get() {
        $this->response(['error' => 'Invalid request']);
    }

    function index_delete() {
        $this->response(['error' => 'Invalid request']);
    }

    function index_post() {
        $this->response(['error' => 'Invalid request']);
    }

    function index_put() {
        $id = $this->put('id');
        $status = $this->put('status');

        $this->response($this->Post_model->update_status($id, $status));
    }
}