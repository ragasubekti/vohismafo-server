<?php
/**
 * Created by PhpStorm.
 * User: ragasub
 * Date: 7/28/17
 * Time: 6:30 PM
 */

require(APPPATH . 'libraries/REST_Controller.php');

class Post extends \Restserver\Libraries\REST_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->load->model('Post_model');
    }

    function index_put() {
        $id = $this->put('id');
        $status = $this->put('status');
        $this->response($this->Post_model->post_update($id, $status));
    }

    function index_get()
    {
		// sleep(5);
        // TODO: Add start & count !! IMPORTANT
        $this->response($this->Post_model->post_list(0, 10));
    }

    function index_post() {
        $title = $this->post('title');
        $location = $this->post('location');
        $information = $this->post('information');
        $topic = $this->post('topic');
        $subject = $this->post('subject');
        $media = $this->post('media');

        $request = $this->Post_model->create_post($title, $location, $information, $topic, $subject, $media);

        $this->response($request);
    }

    function index_delete($id) {
        $this->response($this->Post_model->delete_post($id));
    }


}