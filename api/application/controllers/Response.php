<?php
/**
 * Created by PhpStorm.
 * User: ragasub
 * Date: 8/11/17
 * Time: 8:55 PM
 */

require(APPPATH . 'libraries/REST_Controller.php');


class Response extends \Restserver\Libraries\REST_Controller {
    public function __construct() {
        parent::__construct();

        $this->load->model('Response_model');
    }
    public function index_post() {
        $post = $this->post('post');
        $comment = $this->post('comment');
        $response = $this->post('response');

        $this->response($this->Response_model->post_response($post, $response, $comment));
    }
    public function index_get() {
        $id = $this->get('id');
    
        $this->response($this->Response_model->get_response($id));
    }
    public function index_delete() {
        $this->response(['error' => 'Invalid request']);        
    }
    public function index_put() {
        $this->response(['error' => 'Invalid request']);        
    }            
}