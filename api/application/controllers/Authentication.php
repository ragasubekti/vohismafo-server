<?php
/**
 * Created by PhpStorm.
 * User: ragasub
 * Date: 7/26/17
 * Time: 8:45 PM
 */
require(APPPATH . 'libraries/REST_Controller.php');

class Authentication extends \Restserver\Libraries\REST_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->load->model('User_model');
    }

    function index_get()
    {
        $this->response(['error' => 'This page cannot accessed directly']);
    }

    function index_post()
    {
        $username = $this->post('username');
        $password = $this->post('password');
        $this->response($this->User_model->user_authenticate($username, $password));
    }
}